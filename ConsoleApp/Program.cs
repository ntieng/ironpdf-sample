﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HandlebarsDotNet;
using IronPdf;

namespace ConsoleApp
{
    /// <summary>
    /// This sample is to generate pdf file using IronPdf dll.
    /// Reference url : https://ironpdf.com/tutorials/html-to-pdf/
    /// 1. install IronPDF from NuGet
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            CreatePDFWithHandleBarsUsingExternalTemplate();
        }

        /// <summary>
        /// 1, Creating a PDF with a HTML String in .NET 
        /// </summary>
        private static void CreatingPDFWithHTMLString()
        {
            // Render any HTML fragment or document to HTML
            var Renderer = new IronPdf.HtmlToPdf();
            var PDF = Renderer.RenderHtmlAsPdf("<body><h1> My First Heading</h1><p>My first paragraph.</p></body>");
            var OutputPath = "HtmlToPDF.pdf";
            PDF.SaveAs(OutputPath);

            // This neat trick opens our PDF file so we can see the result in our default PDF viewer
            System.Diagnostics.Process.Start(OutputPath);
        }

        /// <summary>
        /// 2, Exporting a PDF using Existing HTML URL 
        /// </summary>
        private static void CreatingPDFWithUrl()
        {
            // Create a PDF from any existing web page
            var Renderer = new IronPdf.HtmlToPdf();
            var PDF = Renderer.RenderUrlAsPdf("https://en.wikipedia.org/wiki/Portable_Document_Format");
            PDF.SaveAs("wikipedia.pdf");
            // This neat trick opens our PDF file so we can see the result
            System.Diagnostics.Process.Start("wikipedia.pdf");
        }

        /// <summary>
        /// 2.1, Exporting a PDF using Existing HTML URL with javascript/css
        /// </summary>
        private static void CreatingPDFWithUrlWithCSS()
        {
            // Create a PDF Chart a live rendered dataset using d3.js and javascript
            var Renderer = new HtmlToPdf();

            Renderer.PrintOptions.CssMediaType = PdfPrintOptions.PdfCssMediaType.Print;
            Renderer.PrintOptions.EnableJavaScript = true;
            Renderer.PrintOptions.RenderDelay = 5000; //milliseconds

            var PDF = Renderer.RenderUrlAsPdf("https://bl.ocks.org/mbostock/4062006");

            PDF.SaveAs("chart.pdf");
            // This neat trick opens our PDF file so we can see the result
            System.Diagnostics.Process.Start("chart.pdf");
        }

        /// <summary>
        /// 3, Generating a PDF from an Existing HTML file
        /// </summary>
        private static void CreatingPDFWithExistHTMLFile()
        {
            // Create a PDF from an existing HTML using C#
            var Renderer = new IronPdf.HtmlToPdf();
            //var PDF = Renderer.RenderHTMLFileAsPdf("Assets/TestInvoice1.html");
            var PDF = Renderer.RenderHTMLFileAsPdf("Templates/ResetPassword.html");
            var OutputPath = "Invoice.pdf";
            PDF.SaveAs(OutputPath);

            // This neat trick opens our PDF file so we can see the result in our default PDF viewer
            System.Diagnostics.Process.Start(OutputPath);
        }

        /// <summary>
        /// 4, Adding simple Headers And Footers
        /// </summary>
        private static void CreatingPDFWithHeaderAndFooter()
        {
            // placeholders //
            //{page} for the current page number
            //{total-pages} for the total number of pages in the PDF
            //{url} for the URL of the rendered PDF if rendered from a web page
            //{date} for today's date
            //{time} for the current time
            //{html-title} for the title attribute of the rendered HTML document
            //{pdf-title} for the document title, which may be set via the PrintOptions

            var Renderer = new IronPdf.HtmlToPdf();
            Renderer.PrintOptions.MarginTop = 50;  //millimeters
            Renderer.PrintOptions.MarginBottom = 50;
            Renderer.PrintOptions.CssMediaType = PdfPrintOptions.PdfCssMediaType.Print;
            Renderer.PrintOptions.Header = new SimpleHeaderFooter()
            {
                CenterText = "{pdf-title}",
                //RightText = "{date}",
                DrawDividerLine = true,
                FontSize = 16
            };
            Renderer.PrintOptions.Footer = new SimpleHeaderFooter()
            {
                LeftText = "{date} {time}",
                RightText = "Page {page} of {total-pages}",
                DrawDividerLine = true,
                FontSize = 8
            };
            var PDF = Renderer.RenderUrlAsPdf("https://en.wikipedia.org/wiki/Portable_Document_Format");
            var OutputPath = "Invoice.pdf";
            PDF.SaveAs(OutputPath);
            // This neat trick opens our PDF file so we can see the result
            System.Diagnostics.Process.Start(OutputPath);
        }

        /// <summary>
        /// 5, Adding simple Headers And rich Footers
        /// </summary>
        private static void CreatingPDFWithHeaderAndRichFooter()
        {
            var Renderer = new IronPdf.HtmlToPdf();
            Renderer.PrintOptions.MarginTop = 50;  //millimeters
            Renderer.PrintOptions.MarginBottom = 50;
            Renderer.PrintOptions.CssMediaType = PdfPrintOptions.PdfCssMediaType.Print;
            Renderer.PrintOptions.Header = new SimpleHeaderFooter()
            {
                CenterText = "{pdf-title}",
                DrawDividerLine = true,
                FontSize = 16
            };
            Renderer.PrintOptions.Footer = new HtmlHeaderFooter()
            { HtmlFragment = "<div style='text-align:right'><em style='color:pink'>page {page} of {total-pages}</em></div>" };

            var PDF = Renderer.RenderUrlAsPdf("https://en.wikipedia.org/wiki/Portable_Document_Format");
            var OutputPath = "Invoice.pdf";
            PDF.SaveAs(OutputPath);
            // This neat trick opens our PDF file so we can see the result
            System.Diagnostics.Process.Start(OutputPath);
        }

        /// <summary>
        /// 6, Set Paper option of the pdf file
        /// </summary>
        private static void CreatingPDFWithPaperOptionSetting()
        {
            // Create a PDF from any existing web page
            var Renderer = new IronPdf.HtmlToPdf();

            // print paper setting
            Renderer.PrintOptions.PaperSize = PdfPrintOptions.PdfPaperSize.A4;
            Renderer.PrintOptions.PaperOrientation = PdfPrintOptions.PdfPaperOrientation.Landscape;
            Renderer.PrintOptions.FitToPaperWidth = true;

            var PDF = Renderer.RenderUrlAsPdf("https://en.wikipedia.org/wiki/Portable_Document_Format");
            PDF.SaveAs("wikipedia.pdf");
            // This neat trick opens our PDF file so we can see the result
            System.Diagnostics.Process.Start("wikipedia.pdf");
        }

        /// <summary>
        /// 7, Attaching a Cover Page to a PDF 
        /// </summary>
        private static void CreatingPDFWithCoverPage()
        {
            // Create a PDF from any existing web page
            var Renderer = new IronPdf.HtmlToPdf();

            var PDF = Renderer.RenderUrlAsPdf("https://www.nuget.org/packages/IronPdf/");
            PdfDocument.Merge(new PdfDocument("CoverPage.pdf"), PDF).SaveAs("Combined.Pdf");

            // This neat trick opens our PDF file so we can see the result
            System.Diagnostics.Process.Start("Combined.pdf");
        }

        /// <summary>
        /// 8, Watermark to a PDF 
        /// </summary>
        private static void CreatingPDFWithWaterMark()
        {
            IronPdf.HtmlToPdf Renderer = new IronPdf.HtmlToPdf();
            var pdf = Renderer.RenderUrlAsPdf("https://www.nuget.org/packages/IronPdf");
            pdf.WatermarkAllPages("<h2 style='color:red'>SAMPLE</h2>",
                            PdfDocument.WaterMarkLocation.MiddleCenter, 50, -45, "https://www.nuget.org/packages/IronPdf");
            pdf.SaveAs("Watermarked.pdf");

            System.Diagnostics.Process.Start("Watermarked.pdf");
        }

        /// <summary>
        /// 9, Merge pdf files
        /// </summary>
        private static void CreatingPDFByMerging()
        {
            IronPdf.HtmlToPdf Renderer = new IronPdf.HtmlToPdf();

            var PDFs = new List<PdfDocument>();
            PDFs.Add(PdfDocument.FromFile("wikipedia.pdf"));
            PDFs.Add(PdfDocument.FromFile("Invoice.pdf"));
            PDFs.Add(PdfDocument.FromFile("CoverPage.pdf"));
            PdfDocument PDF = PdfDocument.Merge(PDFs);
            PDF.SaveAs("merged.pdf");

            // Add a cover page
            PDF.PrependPdf(Renderer.RenderHtmlAsPdf("<h1>Cover Page</h1><hr>"));

            // Remove the last page from the PDF and save again
            PDF.RemovePage(PDF.PageCount - 1);

            PDF.SaveAs("merged.pdf");

            // This neat trick opens our PDF file so we can see the result
            System.Diagnostics.Process.Start("merged.pdf");

            // Copy pages 5-7 and save them as a new document.
            PDF.CopyPages(4, 6).SaveAs("exerpt.pdf");

            // This neat trick opens our PDF file so we can see the result
            System.Diagnostics.Process.Start("exerpt.pdf");
        }

        /// <summary>
        /// 10, Encrypt pdf file
        /// </summary>
        private static void CreatingPDFWithPassword()
        {
            IronPdf.HtmlToPdf Renderer = new IronPdf.HtmlToPdf();

            PdfDocument Pdf = PdfDocument.FromFile("merged.pdf");
            Pdf.Password = "P@$$w0rd";
            Pdf.SaveAs("encrypt.pdf");

            // This neat trick opens our PDF file so we can see the result
            System.Diagnostics.Process.Start("encrypt.pdf");
        }

        /// <summary>
        /// 11, Create pdf with dynamic C# objects with internal HTML
        /// Reference url : https://handlebarsjs.com/guide/builtin-helpers.html#if
        /// </summary>
        private static void CreatePDFWithHandleBarsUsingInternalTemplate()
        {
            IronPdf.HtmlToPdf Renderer = new IronPdf.HtmlToPdf();

            string source = @"<div class=""entry""><h1>{{title}}</h1><div class=""body"">{{body}}</div></div>";
            var template = Handlebars.Compile(source);

            var data = new
            {
                title = "My new post",
                body = "This is my first post!"
            };

            var result = template(data);
            Renderer.RenderHtmlAsPdf(result).SaveAs("HandleBars.pdf");

            // This neat trick opens our PDF file so we can see the result
            System.Diagnostics.Process.Start("HandleBars.pdf");
        }

        /// <summary>
        /// 12, Create pdf with dynamic C# objects with external HTML file
        /// Reference url : https://handlebarsjs.com/guide/builtin-helpers.html#if
        /// </summary>
        private static void CreatePDFWithHandleBarsUsingExternalTemplate()
        {
            IronPdf.HtmlToPdf Renderer = new IronPdf.HtmlToPdf();

            var ExistingPDF = File.ReadAllText("Templates/ResetPassword2.html");
            var template = Handlebars.Compile(ExistingPDF);

            // This is to loop object and display
            var data = new
            {
                people = new List<Person>(){
                new Person(){firstname = "Joyce", lastname = "Wright", isDisplay = true},
                new Person(){firstname = "Ernest", lastname = "Cooper", isDisplay = false},
                new Person(){firstname = "Travis", lastname = "Alvarado", isDisplay = true},
            }
            };

            var result = template(data);
            Renderer.RenderHtmlAsPdf(result).SaveAs("HandleBars.pdf");

            // This neat trick opens our PDF file so we can see the result
            System.Diagnostics.Process.Start("HandleBars.pdf");
        }
    }
}
